FROM php:7.3-apache

#RUN apt-get update -y 
#RUN apt-get install git -y 
#RUN docker-php-ext-install pdo pdo_mysql mysqli 
#RUN a2enmod rewrite
#Install Composer
#RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#RUN php composer-setup.php --install-dir=. --filename=composer
#RUN mv composer /usr/local/bin/

#RUN docker-php-ext-install mysqli
#RUN docker-php-ext-enable mysqli

ADD . /var/www/html/
RUN echo "ServerSignature Off" >> /etc/apache2/apache2.conf
RUN echo "ServerTokens Prod" >> /etc/apache2/apache2.conf

#EXPOSE 80
#CMD apachectl -D FOREGROUND
#CMD [“apachectl”, “-D”, “FOREGROUND”]
